describe('CRUD API Testing with Various Status Codes', function() {
  let userId
  
  it('Create User - 201 Created', function() {
    cy.request({
      method: 'POST',
      url: 'https://reqres.in/api/users',
      body: {
        name: 'Lusy Maharani',
        job: 'QA Engineer'
      }
    }).then((response) => {
      expect(response.status).to.eq(201)
      expect(response.body).to.have.property('name', 'Lusy Maharani')
      expect(response.body).to.have.property('job', 'QA Engineer')
      userId = response.body.id
    })
  })

  it('Read User - 200 OK', function() {
    cy.request(`https://reqres.in/api/users/2`)
      .then((response) => {
        expect(response.status).to.eq(200)

      expect(response.body.data).to.have.property('id', 2)
      expect(response.body.data).to.have.property('email', 'janet.weaver@reqres.in')
      expect(response.body.data).to.have.property('first_name', 'Janet')
      expect(response.body.data).to.have.property('last_name', 'Weaver')
      expect(response.body.data).to.have.property('avatar', 'https://reqres.in/img/faces/2-image.jpg')

      expect(response.body.support).to.have.property('url', 'https://reqres.in/#support-heading')
      expect(response.body.support).to.have.property('text', 'To keep ReqRes free, contributions towards server costs are appreciated!')
      })
  })

  it('Update User - 200 OK', function() {
    cy.request({
      method: 'PUT',
      url: `https://reqres.in/api/users/2`,
      body: {
        name: 'Lusy Maharani',
        job: 'Software Developer'
      }
    }).then((response) => {
      expect(response.status).to.eq(200)
      expect(response.body).to.have.property('name', 'Lusy Maharani')
      expect(response.body).to.have.property('job', 'Software Developer')
    })
  })

  it('Delete User - 204 No Content', function() {
    cy.request({
      method: 'DELETE',
      url: `https://reqres.in/api/users/${userId}`,
    }).then((response) => {
      expect(response.status).to.eq(204)
    })
  })

  it('Read Non-Existent User - 404 Not Found', function() {
    cy.request({
      method: 'GET',
      url: 'https://reqres.in/api/unknown/23',
      failOnStatusCode: false
    }).then((response) => {
      expect(response.status).to.eq(404)
    })
  })
})
