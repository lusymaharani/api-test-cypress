# Guide

This is a quick guide to running this automation test. Make sure you have Node.js and npm installed on your PC.

## Step 1: Install Dependencies
Before you can run the tests, you need to install all the project dependencies. Run the following command in your terminal:

```bash
npm install
```

## Step 2: Open Cypress Test Runner

```bash
npx cypress open
```
This will open the Cypress Test Runner, where you can see and run your tests.


## Step 3: Run Tests
In the Cypress Test Runner, click on the test file you want to run.
If you prefer to run tests in headless mode (without the Test Runner UI), you can use the following command:

```bash
npx cypress run
```


